'use strict';

module.exports = {
  swagger: {
    enable: true,
    mountPath: '/docs',
    swaggerFilePath: '/swagger.json',
    enableGoogleFont: false,
    oauth2RedirectUrl: '/'
  }
}